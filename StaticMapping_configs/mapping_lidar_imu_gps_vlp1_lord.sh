## usally, you can just leave this config file just like this, it will work fine
CONFIG_PATH=./config/lidar_imu_gps_default.xml
## if you have a urdf file that contains robot model,
## set it here and the mapping process will not listen to tf topics
URDF_FILE=./urdf/backpack_vlp1_lord.urdf
## the follow 2 items must be set!!!
## the topic name of your pointcloud msg (ros)
POINT_CLOUD_TOPIC=/vlp1/velodyne_points
## the frame id of your pointcloud msg (ros)
POINT_CLOUD_FRAME_ID=vlp201

## the following items are optional
## if you do not have an imu or gps or odom
## just remove the line started with
## imu: -imu -imu_frame_id
## odom: -odom -odom_frame_id
## gps: -gps -gps_frame_id
## and If you got one of these topics
## you MUST provide the tf connection between the one to pointcloud frame
IMU_TOPIC=/nav/filtered_imu/data
IMU_FRAME_ID=imu_link

GPS_TOPIC=/gnss1/fix
GPS_FRAME_ID=gnss1_link

#BAG_FILE=/home/deepforest/Desktop/backp_imu_lidar_2022-11-29-11-58-05.bag

./build/ros_node/static_mapping_node \
  -cfg ${CONFIG_PATH} \
  -pc ${POINT_CLOUD_TOPIC} \
  -pc_frame_id ${POINT_CLOUD_FRAME_ID} \
  -imu ${IMU_TOPIC} \
  -imu_frame_id ${IMU_FRAME_ID} \
  -gps ${GPS_TOPIC} \
  -gps_frame_id ${GPS_FRAME_ID} \
#  -odom ${ODOM_TOPIC} \
 # -odom_frame_id ${ODOM_FRAME_ID} \
 # -urdf ${URDF_FILE} \

  #-bag ${BAG_FILE} \
  #-pubs [map][path][edge][submap]

exit 0 

