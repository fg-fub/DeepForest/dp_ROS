#!/usr/bin/env python

from pickle import TRUE
import rospy
from sensor_msgs.msg import PointCloud2 as pc2
from geometry_msgs.msg import PoseWithCovarianceStamped
import tf

class Velo2tf():
	def __init__(self):
		self.first_msg_received = True
		self.base_link_pose = PoseWithCovarianceStamped()
		self.br = tf.TransformBroadcaster()
		#self.tfPub = rospy.Publisher("/veloBroadcaster", TFMessage, queue_size=2)
		#self.pcSub = rospy.Subscriber("/lidar/velodyne_points", PointCloud2, self.pcCallback)
		
	def toQuaternion(self,	yaw, pitch, roll):
		# Abbreviations for the various angular functions
		cy = cos(yaw * 0.5)
		sy = sin(yaw * 0.5)
		cp = cos(pitch * 0.5)
		sp = sin(pitch * 0.5)
		cr = cos(roll * 0.5)
		sr = sin(roll * 0.5)

		q = tf.Quaternion(0, 0, 0, 0)
		q.setW(cy * cp * cr + sy * sp * sr)
		q.setX(cy * cp * sr - sy * sp * cr)
		q.setY(sy * cp * sr + cy * sp * cr)
		q.setZ(sy * cp * cr - cy * sp * sr)
		return q

#	def pcCallback(self,data):
#		tf_out = self.br.sendTransform((self.pcSub.x, self.pcSub.y, self.pcSub.z),
#                     "/velodyne",
#                     "/base_link")
#               
#               self.tfPub.publish(tf_out)
               
	def broadcaster(self):
			    	
		br = tf.TransformBroadcaster()
		transform_base_link = tf.Transform()
		transform_laser = tf.Transform()

		rospy.subscribe("/initialpose", 5, (self.base_link_pose, self.first_msg_received))

		rate = rospy.rate(50.0)

		while rospy.ok():

			if first_msg_received:

				# base frame rotation and origin
				transform_base_link.setRotation(tf.Quaternion(base_link_pose.pose.pose.orientation.x,
															base_link_pose.pose.pose.orientation.y,
															base_link_pose.pose.pose.orientation.z,
															base_link_pose.pose.pose.orientation.w));
				transform_base_link.setOrigin(tf.Vector3(base_link_pose.pose.pose.position.x,
										base_link_pose.pose.pose.position.y,
										base_link_pose.pose.pose.position.z));

				# laser frame rotation and origin
				transform_laser.setRotation(toQuaternion(0.0, 0.0, 0.0));
				transform_laser.setOrigin(tf.Vector3(0, 0, 0.75));

			else:

				# base frame rotation and origin
				transform_base_link.setRotation(toQuaternion(0.0, 0.0, 0.0));
				transform_base_link.setOrigin(tf.Vector3(0, 0, 0));

				# laser frame rotation and origin
				transform_laser.setRotation(toQuaternion(0.0, 0.0, 0.0));
				transform_laser.setOrigin(tf.Vector3(0, 0, 0.75));
			

			br.sendTransform(tf.StampedTransform(transform_base_link, rospy.Time.now(), "/map", "/base_link"))
			br.sendTransform(tf.StampedTransform(transform_laser, rospy.Time.now(), "/base_link", "/velodyne"))

			rospy.spinOnce()

			rate.sleep()
		

		rospy.spin()

		return
	



if __name__ == '__main__':
	rospy.init_node("base_link_broadcaster")
	v2tf = Velo2tf()
	rospy.spin()
	
#Source: https://github.com/sigmaai/self-driving-golf-cart/blob/master/ros/src/navigation/mapping/src/robot_pose_publisher.cpp#L76
