#!/usr/bin/env python3


# adapted from tutorial by "The Construct" and example on laser_assembler documentation

import roslib; roslib.load_manifest('laser_assembler')
import rospy; from laser_assembler.srv import *
from sensor_msgs.msg import PointCloud2

rospy.init_node("assemble_client")
rospy.wait_for_service("/assemble_scans2")
assemble_cloud = rospy.ServiceProxy('/assemble_scans2', AssembleScans2)
pub = rospy.Publisher("/vlp2_cloud", PointCloud2, queue_size=1)
r= rospy.Rate(1)

while not rospy.is_shutdown():
	try:
	    resp = assemble_cloud(rospy.Time(0,0), rospy.get_rostime())
	    print("Got cloud with %u points" % len(resp.cloud.data))
	    pub.publish(resp.cloud)
	    
	except rospy.ServiceException(e):
	    print("Service call failed: %s" %e)
	    
	r.sleep() 
