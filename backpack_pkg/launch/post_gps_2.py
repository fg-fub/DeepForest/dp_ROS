#!/usr/bin/env python

#sources:   gps_wifi.py from MicaSense integration guides
#           ROS Guides

import requests
import rospy
from nmea_msgs.msg import Sentence


class PostGPS():

    def __init__(self, laser_topic="/lidar/gpsimu_driver/nmea_sentence"):

        self.laser_topic = laser_topic
        self._check_laser_ready()
        laser_sub = rospy.Subscriber(self.laser_topic, Sentence, self.laser_callback)
        self.rate = 0.5

    def _check_laser_ready(self):
        laser_msg = None
        rospy.logdebug("Waiting for "+str(self.laser_topic)+" to be READY...")
        while laser_msg is None and not rospy.is_shutdown():
            try:
                laser_msg = rospy.wait_for_message(self.laser_topic, Sentence, timeout=1.0)
                rospy.logdebug("Current "+str(self.laser_topic)+"READY=>")
                rospy.loginfo("receiving GPS information")
            except:
                rospy.logerr("Current "+str(self.laser_topic)+" not ready yet, retrying for getting GPS")
        
        self.laser_sentence = laser_msg.sentence

    def laser_callback(self, msg):  #extract GPS information from Laser and convert NMEA Sentence to single information
        
        '''
        DESCRIPTION OF NMEA SENTENCE VALUES WITH EXAMPLE, only using latitude, longitude and time
        type: $GPRMC Recommended Minimum sentence
        time: 123519 Fix taken at 12:35:19 UTC
        status: A Receiver status: A = Active, V = Void
        lat: 4807.038,N Latitude 48 deg 07.038' N
        n_s: North / South direction 
        long: 01131.000,E Longitude 11 deg 31.000' E
        e_w: East / West direction
        speed: 022.4 Speed over the ground (knots)
        course: 084.4 Track made good (degrees True)
        date: 230394 23rd of March 1994
        magnetic_var_num and magnetic_var: 003.1,W Magnetic Variation
        rest: *6A * followed by 2-byte Checksum
        '''
        
        self.laser_sentence = msg.sentence
        type, time, status, latitude, n_s, longitude, e_w, speed, course, date, magnetic_var_num, magnetic_var, rest = self.laser_sentence.split(',')
        self.latitude, self.longitude, self.time = latitude, longitude, time
        
        # Sleep for a while before publishing new messages. Division is so rate != period.
        if self.rate:
            rospy.sleep(1/self.rate)
        else:
            rospy.sleep(0.5)
		
        
        #rospy.logout("Lat "+self.lat+" Long "+self.long+" Time "+time)
	
    
    def post_state(self):           # Send Lat and Long Information from Laser to Camera via http
        #explanation: https://support.micasense.com/hc/en-us/articles/360044198153
        rospy.loginfo("Starting publishing to camera...")
        capture_arg = "{“use_post_capture_state”:true}"
        capture_data = requests.post("http://192.168.10.254/capture", json=capture_arg)
        rospy.loginfo("'http://192.168.10.254/capture' was send")

        state_params = {"latitude":self.latitude,
                "longitude":self.longitude,
                #"altitude":imu_alt,
                #"utc_time":"2019-12-02T12:34:56.789",
                #"camera_phi":imu_phi,"camera_theta":imu_theta,"camera_psi":imu_psi
                }
        capture_data = requests.post("http://192.168.10.254/capture_state", json=state_params)
        print(capture_data.json())
        rospy.loginfo("'192.168.10.254/capture_state' was send")

        rospy.loginfo("GPS was send to camera successfully")


if __name__ == "__main__":
    rospy.init_node('post_gps')
    try:
        post = PostGPS()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass



