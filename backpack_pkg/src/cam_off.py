import requests

import rospy



def end_capture():
        rospy.loginfo("Camera is ending auto capture.")
        stop_capture = requests.post("http://192.168.1.83/timer?enable=false")
        print(stop_capture.json())
        shutdown = requests.post("http://192.168.1.83/powerdownready?power_down=true")
        print(shutdown.json())   

if __name__ == '__main__':
  try:
        end_capture()      

  except rospy.ROSInterruptException: 

        pass

 
 
