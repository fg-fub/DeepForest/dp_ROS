#!/usr/bin/env python

import requests
from requests.adapters import HTTPAdapter
from requests.exceptions import ConnectionError
import rospy

from sensor_msgs.msg import CameraInfo

class reportInfos(): 
    def __init__(self):
        
        pub_1 = rospy.Publisher('/msc/info_band1', CameraInfo, queue_size=1)
        pub_2 = rospy.Publisher('/msc/info_band2', CameraInfo, queue_size=1)
        pub_3 = rospy.Publisher('/msc/info_band3', CameraInfo, queue_size=1)
        pub_4 = rospy.Publisher('/msc/info_band4', CameraInfo, queue_size=1)
        pub_5 = rospy.Publisher('/msc/info_band5', CameraInfo, queue_size=1)
        self.pub_list = [pub_1, pub_2, pub_3, pub_4, pub_5]
        rospy.init_node('msc_bands')
        
        self.t = 2  #timeout, to wait until connection is established, in seconds
        self.adapter = HTTPAdapter(max_retries=5)  # will retry a connection after it failed
        self.session = requests.Session()
        self.session.mount("http://192.168.1.83/", self.adapter)
        
        # GET camera information and distortion
        try: 
            self.capture_infos = self.session.get("http://192.168.1.83/camera_info", timeout=self.t)
            self.capture_distortion = self.session.get("http://192.168.1.83/calibration/distortion", timeout=self.t)
            self.talker()
        except ConnectionError as ce:
            rospy.logwarn(ce)

        
    # Convert JSON information into ROS messages
    def convertJSON2CameraInfo(self,cam_info, cam_dist, bandnumber):
        '''
        height_1, width_1: image dimensions of band 1
        distortion_model_1: distortion model for band 1 used. Supported models are listed in sensor_msgs/distortion_models.h
        D_plumb_bob_1 = plump blob distortion model for band 1. For "plumb_bob", the 5 parameters are: (k1, k2, t1, t2, k3).
        intrinsic_1 = Intrinsic camera matrix for the raw (distorted) images.
                     [fx  0 cx]
                 K = [ 0 fy cy]
                     [ 0  0  1]
                    Projects 3D points in the camera coordinate frame to 2D pixel coordinates using the focal lengths (fx, fy) and principal point (cx, cy).
    '''
        b = str(bandnumber)
        #header_b = capture_infos.json()[b]['bandname']
        height_b = int(cam_info.json()[b]['image_height'])
        width_b = int(cam_info.json()[b]['image_width'])
        distortion_model_b = 'plumb_bob'+'_'+cam_info.json()[b]['bandname']
        D_plumb_bob_b = [cam_dist.json()[b]['p'][0], cam_dist.json()[b]['p'][1], 
                        cam_dist.json()[b]['k'][0], cam_dist.json()[b]['k'][1], cam_dist.json()[b]['k'][2]]
        D_plumb_bob_b = [float(item) for item in D_plumb_bob_b]

        intrinsic_b = [cam_dist.json()[b]['fx'], 0, cam_dist.json()[b]['cx'], 
                        0, cam_dist.json()[b]['fy'], cam_dist.json()[b]['cy'], 
                        0,0,1]
        intrinsic_b = [float(item) for item in intrinsic_b]
        return height_b, width_b, distortion_model_b, D_plumb_bob_b, intrinsic_b


    def talker(self):

        while not rospy.is_shutdown() :
            for b in range(5):
                json = self.convertJSON2CameraInfo(self.capture_infos, self.capture_distortion, b+1)
                self.pub_list[b].publish(height=json[0], width=json[1], distortion_model=json[2], D=json[3], K=json[4])
            rospy.sleep(10.0)


if __name__ == '__main__':
    
    try:
        reportInfos()
    except rospy.ROSInterruptException:
        pass


#### Raw Output of Camera JSON ###
'''
>> 03.02.2023

-------------camera_info---------------
{
'1': {'bandname': 'Blue', 'bandwidth_nm': 32, 'center_nm': 475, 'focal_length_px': 1466.666666666667, 'image_height': 960, 'image_width': 1280, 'type': 'bandpass'}, 
'2': {'bandname': 'Green', 'bandwidth_nm': 27, 'center_nm': 560, 'focal_length_px': 1466.666666666667, 'image_height': 960, 'image_width': 1280, 'type': 'bandpass'}, 
'3': {'bandname': 'Red', 'bandwidth_nm': 14, 'center_nm': 668, 'focal_length_px': 1466.666666666667, 'image_height': 960, 'image_width': 1280, 'type': 'bandpass'}, 
'4': {'bandname': 'NIR', 'bandwidth_nm': 57, 'center_nm': 842, 'focal_length_px': 1466.666666666667, 'image_height': 960, 'image_width': 1280, 'type': 'bandpass'}, 
'5': {'bandname': 'Red edge', 'bandwidth_nm': 12, 'center_nm': 717, 'focal_length_px': 1466.666666666667, 'image_height': 960, 'image_width': 1280, 'type': 'bandpass'}
}
------------calibration/distortion----------------
{
'1: {'cx': 632.5075, 'cy': 488.2897, 'fx': 1451.736, 'fy': 1451.7795, 'k': [-0.1047797, 0.1936219, -0.1258823], 'p': [0.0009945083, -0.0002144772]}, 
'2': {'cx': 628.5667, 'cy': 478.4457, 'fx': 1445.8885, 'fy': 1445.7964, 'k': [-0.1064943, 0.169645, -0.07656262], 'p': [0.001048874, -0.0004467681]}, 
'3': {'cx': 634.9805, 'cy': 478.7468, 'fx': 1451.3264, 'fy': 1451.1871, 'k': [-0.1100523, 0.1903069, -0.1222212], 'p': [0.0003323937, -0.0008141776]}, 
'4': {'cx': 642.1021, 'cy': 478.8695, 'fx': 1445.3117, 'fy': 1445.3297, 'k': [-0.1120625, 0.1891504, -0.1246147], 'p': [0.0005723328, -0.0002671686]}, 
'5': {'cx': 627.0835, 'cy': 490.9755, 'fx': 1445.6292, 'fy': 1445.5382, 'k': [-0.1115623, 0.1922929, -0.1185325], 'p': [0.0005413496, 0.0002610356]}
}



'''


