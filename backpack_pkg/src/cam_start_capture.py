import requests

from argparse import ArgumentParser

class StartEndCam:

    parser = ArgumentParser()
    parser.add_argument("--autocapture", type=bool, help="Choose if camera auto capture mode starts or stops", default=False)
    parser.add_argument("--shutdown", type=bool, help="Choose if camera should prepare for shutdown (detaches storage)", default=False)
    args = parser.parse_args()
    print(args.autocapture, args.shutdown)

    if args.autocapture == True:
        # Post a message to the camera commanding a capture, block until complete
        capture_params = { 'store_capture' : True, 'block' : True, 'use_post_capture_state': True , 'preview': True} ## use cached jpeg to show in rviz
        config_params = { 'auto_cap_mode' : 'timer', 'timer_period' : 1.0}
        requests.post("http://192.168.1.83/config", json=config_params)
        requests.post("http://192.168.1.83/capture", json=capture_params)
        capture = requests.post("http://192.168.1.83/timer?enable=true")
        print(capture.json())
        while True:
            status = requests.get("http://192.168.1.83/status")
            print(status.json())

    else:
        stop_capture = requests.post("http://192.168.1.83/timer?enable=false")
        print(stop_capture.json())
        
    if args.shutdown ==True:    
        shutdown = requests.post("http://192.168.1.83/powerdownready?power_down=true")
        print(shutdown.json())
    else: 
        pass


