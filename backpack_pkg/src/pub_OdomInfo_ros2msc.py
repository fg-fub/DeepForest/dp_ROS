#!/usr/bin/env python

import requests
from requests.adapters import HTTPAdapter
from requests.exceptions import ConnectionError

import datetime
import math
import rospy
from nav_msgs.msg import Odometry
from std_msgs.msg import String


class ros2camera:

    def __init__(self):
    
        #for connection with ROS
        rospy.init_node('inform_camera')
        self.rate = rospy.Rate(1)
        
        #for connection to HTTP
        self.t = 200  #timeout, to wait until connection is established, in seconds
        self.adapter = HTTPAdapter(max_retries=5)  # will retry a connection after it failed
        self.session = requests.Session()
        self.session.mount("http://192.168.1.83/", self.adapter)
        try:
            status = self.session.get("http://192.168.1.83/status", timeout=self.t)
            rospy.loginfo(str(status.json()))
            capture_params = { 'store_capture' : True, 'block' : True, 'use_post_capture_state': True , 'preview': True}
            config_params = { 'auto_cap_mode' : 'timer', 'timer_period' : 1.0,}
            self.session.post("http://192.168.1.83/config", json=config_params, timeout=self.t)
            self.session.post("http://192.168.1.83/capture", json=capture_params, timeout=self.t)
            capture = self.session.post("http://192.168.1.83/timer?enable=true", timeout=self.t)
            rospy.loginfo("Camera is starting auto capture.")
            rospy.loginfo(str(capture.json()))
        except ConnectionError as ce:
            rospy.logwarn(ce)
        
        while not rospy.is_shutdown() :
          rospy.Subscriber("/nav/odom", Odometry, self.sending2camera)
          self.pub = rospy.Publisher("/msc/msc_send_status", String, queue_size=10)
        rospy.on_shutdown(self.end_capture)


    def quaternion2euler(self, x,y,z,w):
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        roll_x = math.atan2(t0, t1)
     
        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        pitch_y = math.asin(t2)
     
        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        yaw_z = math.atan2(t3, t4)
     
        return roll_x, pitch_y, yaw_z # in radians
        

    def sending2camera(self, data):
      
        # position
        latitude = data.pose.pose.position.x
        latitude = math.radians(latitude)
        longitude = data.pose.pose.position.y
        longitude = math.radians(longitude)
        altitude = data.pose.pose.position.z
        
        #orientation
        x = data.pose.pose.orientation.x
        y = data.pose.pose.orientation.y
        z = data.pose.pose.orientation.z
        w = data.pose.pose.orientation.w

        phi, theta, psi = self.quaternion2euler(x,y,z,w)

        utc_time = datetime.datetime.utcnow()
        utc_time = "{}-{:02d}-{:02d}T{}:{}:{}.{}".format(utc_time.year, utc_time.month, utc_time.day, utc_time.hour, utc_time.minute, utc_time.second, str(utc_time.microsecond)[:3])
        
        

        ################# sending to camera
        
        position_params = { 'aircraft_phi' : phi,
                            'aircraft_theta' : theta,
                            'aircraft_psi' : psi, 
                            'camera_phi' : -1.5707963,
                            'camera_theta' : -1.5707963,
                            'camera_psi' : 0.0,
                            'latitude' : latitude, 
                            'longitude': longitude, 
                            'altitude': altitude, 
                            'utc_time' : utc_time
                            }
        rospy.loginfo("sending the information to the camera ...")
        try:
            capture_data_time = self.session.post("http://192.168.1.83/capture_state", json=position_params, timeout=self.t)
        except ConnectionError as ce:
            rospy.logwarn(ce)
        txt = str(capture_data_time.json())
        self.pub.publish(txt)
        txt = None

        
        # orientation_params = { 'aircraft_phi' : phi,
        #                     'aircraft_theta' : theta,
        #                     'aircraft_psi' : psi, 
        #                     'camera_phi' : -1.5707963,
        #                     'camera_theta' : -1.5707963,
        #                     'camera_psi' : 0,
                            
        #                     }
        # #print("sending the information to the camera ...")
        # capture_data_time = requests.post("http://192.168.1.83/orientation", json=orientation_params)
        # print(capture_data_time.json())

        try:
            cam_status = self.session.get("http://192.168.1.83/status", timeout=self.t)
        except ConnectionError as ce:
            rospy.logwarn(ce)
        
        self.pub.publish("Currently camera SD Storage left: ", str(cam_status.json()["sd_gb_free"])[:7], " / ", str(cam_status.json()["sd_gb_total"])[:7])
        cam_sd_red = cam_status.json()["sd_warn"]
        if cam_sd_red == True: 
          rospy.logwarn("SD Card of red camera almost full!")

        self.rate.sleep()
        
    def end_capture(self):
        rospy.loginfo("Camera is ending auto capture.")
        try:
            stop_capture = self.session.post("http://192.168.1.83/timer?enable=false", timeout=self.t)
            txt = str(stop_capture.json())
            rospy.loginfo(txt)
            txt = None
            
        except ConnectionError as ce:
            rospy.logwarn(ce)
        

if __name__ == '__main__':
  try:
        ros2camera()

  except rospy.ROSInterruptException: 
        pass

 
  


# -----------------


'''
OUTPUT OF /imu/nav/odom 

header: 
  seq: 1671
  stamp: 
    secs: 1675513527    #TIME
    nsecs: 360754406
  frame_id: "sensor_wgs84"
child_frame_id: "sensor"
pose: 
  pose: 
    position: 
      x: 49.862092389858105   #GPS lat
      y: 8.679037934163667         #GPS LONG
      z: 240.8105145785355      # GPS ALT
    orientation: 
      x: 0.0018981278408318758
      y: -0.00014660877059213817
      z: 0.04816616699099541
      w: 0.9988375306129456
  covariance: [3582.8365157387598, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3582.8365157387598, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3582.8365157387598, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.973756077432382e-06, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.9469057083309365e-06, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0059109938767898385]
twist: 
  twist: 
    linear: 
      x: -0.15309178829193115
      y: 0.09651438146829605
      z: 0.2371663600206375
    angular: 
      x: 0.00015465420437976718
      y: 0.0018760506063699722
      z: -0.0005795593024231493
  covariance: [5.159766909499922, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5.154747770559936, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3.9731549150776715, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]



'''


