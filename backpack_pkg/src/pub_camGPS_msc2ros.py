#!/usr/bin/env python

import requests

import rospy
#from rospy_message_converter import json_message_converter
from sensor_msgs.msg import NavSatFix

pub = rospy.Publisher('gps', NavSatFix, queue_size=1)

rospy.init_node('msc_gps_data')

# Convert JSON information into ROS messages
def convertJSON2NavSatFix(data):

    latitude_b = float(data.json()['latitude'])
    longitude_b = float(data.json()['longitude'])
    altitude_b = float(data.json()['altitude'])

    return latitude_b, longitude_b, altitude_b


def talker():
    while not rospy.is_shutdown() :
        gps_data = requests.get('http://192.168.1.83/gps')   #Read the GPS data from the camera
        json = convertJSON2NavSatFix(gps_data)
        pub.publish(latitude=json[0], longitude=json[1], altitude=json[2])
        rospy.sleep(1.0)



if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException: pass


#### Raw Output of Camera JSON ###

'''
>> 03.02.2023
Raw data : {"altitude":0,"fix3d":false,"latitude":0,"longitude":0,"p_acc":0,"utc_time":"1970-01-01T00:00:00Z","utc_time_valid":false,"v_acc":0,"vel_d":0,"vel_e":0,"vel_n":0}


'''


