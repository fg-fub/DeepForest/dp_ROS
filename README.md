# Overview

**Warning: Currently in Alpha**

This package launches multiple sensors on a backpack system. With launch, the nodes of the sensors will start and it will open a rviz window to show each sensor and coordinate systems (CS). Additionally, the sensor data can be saved in bag files, default setting is set to false.

In more detail: 
The launch file backpack.launch includes the backpack_sensors.launch and also starts the static CS transformations between the sensors, and optionally the SLAM calculations based on the Lidar data. The latter receives GPS information. Lastly a RVIZ window can be launched.

The launch file backpack_sensors.launch starts the nodes for the Velodyne lidar and the Xsens/Microstrain IMU each in their namespaces for better understanding of the affiliations when listing active topics. 

The currend Includes lidar sensor Velodyne VLP16 and IMU sensor Microstrain 3DM-GQ7 and the past version IMU Xsens GTI-700.
It was tested on Ubuntu 20.04 and ROS noetic. 

The backpack also includes a multispectral camera from MicaSense. Communication scripts can be found under "src". 

Overview of the hardware connections of the backpack system this code was based on:

![Alt text](./Connection_Graph_backpack.svg)

Overview of the connections between the ROS packages and nodes. Note that "lidar1" is used as example and was also implemented the same way for lidar2. 

![Alt text](./Connection_ROS_backpack.svg)

Visualisation of the backpack that the documents were written and customised for.  

![Alt text](./Visualisation_backpack.jpeg)

# Dependencies
please install everything with

```
cd your/catkin_ws
rosdep check backpack_pkg #to see what is needed
rosdep install backpack_pkg

```

Along some standard packages, you will need the following. These will also be installed with rosdep. 
#### LiDAR
- velodyne for ROS noetic
	more information here
	http://wiki.ros.org/velodyne
	http://wiki.ros.org/velodyne/Tutorials/Getting%20Started%20with%20the%20Velodyne%20VLP16
#### IMU			      
- microstrain_inertial_driver for ROS noetic
	more information https://github.com/LORD-MicroStrain/microstrain_inertial

OR

- xsens_ros_mti_driver for ROS noetic
	more information http://wiki.ros.org/xsens_mti_driver
#### SLAM
- StaticMapping
	https://github.com/EdwardLiuyc/StaticMapping

OR

- FastLIO2
	https://github.com/hku-mars/FAST_LIO

# Instructions
 
to launch package including navigationa and connected sensors, while also recording data in bag file, run the following in directory of catkin workspace:
```
$ source devel/setup.bash
$ roslaunch backpack_pkg backpack.launch record:=true
```
to launch sensors and not record data in bag file:
```
$ source devel/setup.bash
$ roslaunch backpack_pkg backpack_sensors.launch
```
		
bag files will be saved in backpack_pkg/bagfiles with their date and time, such as: 
		`backp_imu_lidar_2021-12-22-13-29-33.bag`
			
			
to play a bag file launch without recording and type in a new terminal: 
```
$ rosbag play <path/to/rosbag-name>.bag
```
example:
```
$ rosbag play rosbag_src/bagfiles/backp_imu_lidar_2021-12-22-13-29-33.bag
```
more information: http://wiki.ros.org/rosbag/Commandline


After creating a catkin workspace, installing the other to packages and downloading this repo, make sure to 	
- activate the backpack_pkg by using catkin-make pkg backpack_pkg
- go back to the home of your catkin workspace and use "catkin_make" and "rosdep update"

# Paper

* Segmentation of Forest Vegetation Layers Based
on Geometric Features Extracted from 3D Point Clouds: [Goebel et al. (2023)](https://gispoint.de/artikelarchiv/avn/2023/avn-ausgabe-052023/7881-segmentierung-der-waldvegetationsschichten-anhand-von-geometrischen-merkmalen-aus-3d-punktwolken.html)

* Backpack System for Capturing 3D Point Clouds of Forests,
postprint: [Goebel & Iwaszczuk (2023)](https://www.researchgate.net/publication/375160087_Backpack_System_for_Capturing_3D_Point_Clouds_of_Forests)


# Contact

If you do not have a GitLab account, you can write me any issues, ideas and comments to: 
mona.goebel@tu-darmstadt.de
